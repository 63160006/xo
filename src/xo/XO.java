/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xo;

import java.util.Scanner;

/**
 *
 * @author tud08
 */
public class XO {

    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char player = 'O';
    static int row;
    static int col;
    static boolean finish = false;
    static int count = 0;

    public static void main(String[] args) {

        showWelcome();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            process();
            if (finish) {
                break;
            }
        }
    }

    public static void showTable() {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                System.out.print(table[i][j]);
            }
            System.out.println("");
        }
    }

    public static void showWelcome() {
        System.out.println("Welcome to XO Game");
    }

    public static void showTurn() {
        System.out.println("Turn " + player);
    }

    private static void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Please input row, col : ");
        row = kb.nextInt();
        col = kb.nextInt();
    }

    public static void process() {
        if (setTable()) {
            if (checkWin()) {
                finish = true;
                showTable();
                System.out.println(">>> " + player + " Win <<<");
                return;
            }
            if (checkDraw()) {
                finish = true;
                showTable();
                showDraw();
                return;
            }
            count++;
            swichPlayer();
        }
    }

    public static void swichPlayer() {
        if (player == 'O') {
            player = 'X';
        } else {
            player = 'O';
        }
    }

    public static boolean setTable() {
        table[row - 1][col - 1] = player;
        return true;
    }

    private static boolean checkWin() {
        if (checkVertical()) {
            return true;
        } else if (checkHorizontal()) {
            return true;
        } else if (checkX()) {
            return true;
        }
        return false;
    }

    private static boolean checkVertical() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][col - 1] != player) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkHorizontal() {
        for (int j = 0; j < table.length; j++) {
            if (table[row - 1][j] != player) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX() {
        if (checkX1()) {
            return true;
        } else if (checkX2()) {
            return true;
        }
        return false;
    }

    private static boolean checkX1() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != player) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != player) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDraw() {
        if (count==8) {
            return true;
    
        }return false;
    }

    private static void showDraw() {
        System.out.println(">>> Draw <<<");
    }
}
